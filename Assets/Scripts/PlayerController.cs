﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static GameManager instance;

    Transform tf; // stores Transform in string tf;
    public float forwardSpeed; // creates a public float called speed in the inspector
    public float reverseSpeed; // creates a public varaible in the inspector for user input
    public float rotateSpeed; // creates a public float called rotationSpeed in the inspector
       
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // used tf for Transform and sets it equal to compnenet Transform. 
    }

    // Update is called once per frame
    void Update()
    {
        SimpleMove(); // calls the function
        Rotate(); // calls the function

    }
    
    public void SimpleMove()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) // player pushes W or Up and plyer moves forward
        {
            tf.transform.Translate(Time.deltaTime * forwardSpeed, 0, 0); // this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) // player pushes S or Down and plyer moves Backwards
        {
            tf.transform.Translate(-Time.deltaTime * reverseSpeed, 0, 0);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
    }

    public void Rotate()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) // player pushes A or Left and plyer moves Left
        {
            tf.transform.Rotate(0, 0, Time.deltaTime * rotateSpeed);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) // player pushes D or Right and plyer moves Right
        {
            tf.transform.Rotate(0, 0, -Time.deltaTime * rotateSpeed);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
    }

}
