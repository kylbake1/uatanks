﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D bullet) //  if enemyship collider is triggered
    {
        if (bullet.gameObject.tag == "EnemyBullet") // by a bullet with tag Bullet
        {
            Destroy(this.gameObject);// and is destroyed 

            Destroy(bullet.gameObject);// Also destroy bullet
        }
    }
}
