﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public static GameManager instance; // static so it can be accessed from any script 


    void Start()
    {
        Destroy(this.gameObject, GameManager.instance.bulletLife); // bullet is destroyed after user desired set time limit
    }

    void OnTriggerEnter2D(Collider2D bullet) //  creates a trigger for the bullet
    {
        if (bullet.gameObject.tag == "Wall") // if the bullet makes contact with the wall tagged "Wall"
        {
            Destroy(this.gameObject);// Destroy the bullet
        }

        if (bullet.gameObject.tag == "FirePoint") GetComponent<Rigidbody2D>().AddForce(transform.right * GameManager.instance.bulletSpeed); // this will allow the bullet to shoot from the designated firepoint from the player tank
        if (bullet.gameObject.tag == "EnemyFirePoint") GetComponent<Rigidbody2D>().AddForce(transform.right * GameManager.instance.bulletSpeed); // this will allow the bullet to shoot from the designated firepoint from the enemy tank

    }

}