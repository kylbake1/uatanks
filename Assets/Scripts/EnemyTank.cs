﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : MonoBehaviour
{
    public static GameManager instance; // static so it can be accessed from any script 
    public GameObject bullet; // bullet prefab attachment
    public Transform firePoint; // firepoint prefab attachment
    public bool shoot = false; // bool that is set to false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        GameManager.instance.enemyShotDelayCounter -= Time.deltaTime; // this will reduce the cooldown time when the enemyshoots by using the time from unity
        EnemyShoot(); // use the enemy shoot function
    }

    void OnTriggerEnter2D(Collider2D bullet) //  if enemyship collider is triggered
    {
        if (bullet.gameObject.tag == "Bullet") // by a bullet with tag Bullet
        {
            Destroy(this.gameObject);// and is destroyed             
            Destroy(bullet.gameObject);// Also destroy bullet

        }
    }

    public void EnemyShoot()
    {
        if (shoot == true) // if the shoot bool is true
        {
            if (shoot) // if shoot is true
            {
                if (GameManager.instance.enemyShotDelayCounter <= 0) // if the enemy cooldown counter is less than or equal to 0
                {
                    GameManager.instance.enemyShotDelayCounter = GameManager.instance.fireRate; // cool down timer is increased by the set number from user 
                    Instantiate(bullet, firePoint.position, firePoint.rotation); // bullet is then fired crom the firepoint and rotated in the x axis from firepoint direction
                }
            }
        }
    }



}
