﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawn : MonoBehaviour
{
    public static GameManager instance; // static so it can be accessed from any script 
    public GameObject bullet; // bullet prefab attachment
    public Transform firePoint; // firepoint prefab attachment
    private bool UseInput = true;// private bool that is set to true;
    private Animator anim; //private Animator that is called anim


    void Start()
    {
        anim = GetComponent<Animator>(); //automatically fills anim with animator
    }

    private void FixedUpdate()
    {
        GameManager.instance.shotDelayCounter -= Time.deltaTime; // shot delay counter is reduced by unity time
        bool pshoot = false; // pshoot is set to false
        if (anim.GetBool("isShooting")) // get the bool from animator called isShooting
            anim.SetBool("isShooting", false); // set the bool for isShooting to false to stop the animation
        if (UseInput == true) // if useinput is true
        {
            pshoot = Input.GetButton("Fire1"); // shoot is set to false once we press the space bar

        }

        if (pshoot) // if pshoot is true
        {
            if (GameManager.instance.shotDelayCounter <= 0) // if the shot delay counter is less than or equal to 0
            {
                GameManager.instance.shotDelayCounter = GameManager.instance.fireRate; // set the counter back up to the fireRate set by user
                Instantiate(bullet, firePoint.position, firePoint.rotation); // creates a clone of bullet and fires it from firepoint of tank and rotates bullet in x direction of firepoint
                anim.SetBool("isShooting", true); // set the isShooting bool for animator to true
            }

        }
    }
}
