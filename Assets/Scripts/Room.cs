﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour
{

    public GameObject doorNorth; // doorNorth prefab attachment
    public GameObject doorSouth; // doorSouth prefab attachment
    public GameObject doorEast; // doorEast prefab attachment
    public GameObject doorWest; // doorWest prefab attachment
}
