﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Singleton
    public static GameManager instance;

    public GameObject bullet;// Bullet prefab attachment
    public float bulletLife; // editable in inspector
    public float bulletSpeed; // editable in inspector
    public float fireRate; // editable in inspector
    public float enemtFireRate; // editable in inspector

    public float shotDelayCounter; // editable in inspector
    public float enemyShotDelayCounter; // editable in inspector

    public float shellForce; // editable in inspector
    public float damageDone; // editable in inspector
    public float enemyHealth; // editable in inspector
    public float pointsOnDeath; // editable in inspector
    public float playerHealth; // editable in inspector




    public GameObject player; // player prefab attachment

    void Awake()
    {
        if (instance == null) // if instance is equal to nothing
        {
            instance = this; // this instance
            DontDestroyOnLoad(gameObject);// will not be destroyed
        }
        else // otherwise
        {
            Destroy(gameObject); // destroy other gameobjects
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
