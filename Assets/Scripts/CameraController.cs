﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public PlayerController player; // playercontroller is called player
    public bool isFollowing; // bool for camera follow
    public float xOffset; // change the x value
    public float yOffset;// change the y value

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>(); // automatically set the playercontoller to player

        isFollowing = true; // camera is following the player movement
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
            transform.position = new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, transform.position.z); // this will change the postion of the camera to the players position on screen when alive.
    }
}
